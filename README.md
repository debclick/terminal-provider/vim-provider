# Vim

Provides vim to your terminal

## Set up
To make vim provided by this app usable in your terminal run

    echo "export CLICK_DIR=/opt/click.ubuntu.com" >> .bashrc
    echo "export PATH=$PATH:$CLICK_DIR/vim-provider.fuseteam/current/bin" >> .bashrc

and restart the terminal, or run `source ~/.bashrc` to apply immediatly.

## License

Copyright (C) 2020  Jonatan Hatakeyama Zeidler
Copyright (C) 2021  Rahammetoela Toekiman

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
